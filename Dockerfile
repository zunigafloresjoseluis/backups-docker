FROM ubuntu:latest
ENV PASSWDPOSTGRES PASSWDMONGO PASSWDREDIS HOSTPOSTGRES HOSTMONGO HOSTREDIS

WORKDIR home
RUN apt-get update
RUN apt-get install -y gnupg
RUN apt-get install wget -y
RUN wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc |  apt-key add -
RUN echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.2 multiverse" |  tee /etc/apt/sources.list.d/mongodb-org-4.2.list
RUN apt-get update
RUN apt-get install -y mongodb-org

COPY ./postgres/pgsql/*   postgres/pgsql/ 
COPY ./postgres/bash_backup.sh postgres/bash_backup.sh



